from ckan.plugins import implements, SingletonPlugin
from ckan.plugins import IConfigurer, toolkit
from ckanext.saml2auth.interfaces import ISaml2Auth
from flask import g
import logging

class PfrPlugin(SingletonPlugin):
    implements(IConfigurer, inherit=True)
    implements(ISaml2Auth, inherit=True)

    # IConfigurer
    def update_config(self, config):
        logger = logging.getLogger(__name__)
        logger.info('PfrPlugin:update_config ')

        toolkit.add_template_directory(config, 'templates')
        toolkit.add_public_directory(config, 'public')
        toolkit.add_public_directory(config, 'assets')
        toolkit.add_resource('assets', 'pfr')

    # ISaml2Auth
    def after_saml2_login(self, resp, saml_attributes):
        logger = logging.getLogger(__name__)
        logger.info('PfrPlugin:after_saml2_login')
        organization_id = toolkit.config.get('ckanext.pfr.organization.id')
        if not organization_id:
            logger.error("ckanext.pfr.organization.id not set")
            return resp

        try :
            username = g.user
            organization_role = toolkit.config.get('ckanext.pfr.organization.role', 'member')
            logger.info("adding %s to collection %s as %s", username, organization_id, organization_role)
            toolkit.get_action('member_create')(
                context={'ignore_auth': True},
                data_dict={
                    'id': organization_id,
                    'object': username,
                    'object_type': 'user',
                    'capacity': organization_role
                }
            )
        except Exception as e:
            logger.error("Error adding %s to collection %s: %s", username, organization_id, e)
        
        return resp